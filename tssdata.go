package tssdata

import "time"

type TSSticket struct {
	Issuer             int           `json:"issuer,omitempty"`
	TicketID           string        `json:"ticketId,omitempty"`
	ValidFrom          time.Time     `json:"validFrom,omitempty"`
	ValidTill          time.Time     `json:"validTill,omitempty"`
	IssuingDate        time.Time     `json:"issuingDate,omitempty"`
	TicketRawData      string        `json:"ticketRawData,omitempty"`
	ProcessingDatetime time.Time     `json:"processingDatetime,omitempty"`
	CertificateVersion string        `json:"certificateVersion,omitempty"`
	Annotations        []Annotations `json:"annotations,omitempty"`
}
type Controller struct {
	DeviceID                    string `json:"deviceId,omitempty"`
	DeviceType                  string `json:"deviceType,omitempty"`
	InspectionStaffID           string `json:"inspectionStaffId,omitempty"`
	TicketControlOrganizationID string `json:"ticketControlOrganizationId,omitempty"`
	TrainCarrier                string `json:"trainCarrier,omitempty"`
}
type Location struct {
	StationCode        string `json:"stationCode,omitempty"`
	CountryCode        string `json:"countryCode,omitempty"`
	LocalStationCode   string `json:"localStationCode,omitempty"`
	StationName        string `json:"stationName,omitempty"`
	TransportserviceID string `json:"transportserviceId,omitempty"`
}
type Annotations struct {
	AnnotationID         string     `json:"annotationId,omitempty"`
	ExternalID           string     `json:"externalId,omitempty"`
	AnnotationType       string     `json:"annotationType,omitempty"`
	TimeCreated          time.Time  `json:"timeCreated,omitempty"`
	CancellationDatetime time.Time  `json:"cancellationDatetime,omitempty"`
	ControlResult        string     `json:"controlResult,omitempty"`
	Controller           Controller `json:"controller,omitempty"`
	Location             Location   `json:"location,omitempty"`
}

func CreateTSSFormatByAgent(issuer int, IssuingDate time.Time,
	TicketID,
	ValidFrom time.Time,
	TicketRawData,
	CertificateVersion string, Annotations []Annotations) *TSSticket {
	Ticket := TSSticket{}

	return &Ticket
}

func SetAnnotationCancelation(
	AnnotationID, ExternalID string, CancellationDatetime time.Time) *Annotations {
	Cancelation := Annotations{}
	Cancelation.AnnotationType = "Cancelation"
	Cancelation.AnnotationID = AnnotationID
	Cancelation.ExternalID = ExternalID
	Cancelation.CancellationDatetime = CancellationDatetime

	return &Cancelation
}

func SetAnnotationControl(
	AnnotationID,
	ExternalID string,
	TimeCreated time.Time,
	ControlResult string,
	DeviceID string,
	DeviceType string,
	InspectionStaffID string,
	TicketControlOrganizationID string,
	TrainCarrier string,
	LocalStationCode string,
	TransportserviceID string) *Annotations {
	Control := Annotations{
		AnnotationID:   AnnotationID,
		ExternalID:     ExternalID,
		AnnotationType: "Control",
		TimeCreated:    TimeCreated,

		ControlResult: ControlResult,
		Controller: Controller{
			DeviceID:                    DeviceID,
			DeviceType:                  DeviceType,
			InspectionStaffID:           InspectionStaffID,
			TicketControlOrganizationID: TicketControlOrganizationID,
			TrainCarrier:                TrainCarrier,
		},
		Location: Location{

			LocalStationCode:   LocalStationCode,
			TransportserviceID: TransportserviceID,
		},
	}

	return &Control

}
